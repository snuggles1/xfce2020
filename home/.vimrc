set number	        
set showbreak=+++	
set textwidth=100
set showmatch	
"set visualbell
set mouse=a " full mouse activation

set hlsearch	    
set smartcase	   
set ignorecase	  
set incsearch	 

" nicer vertical separator
set fillchars+=vert:│ 
hi VertSplit cterm=NONE

"set smartindent	
"set smarttab	
"set softtabstop=4

" Better tabsize
set tabstop=4
set shiftwidth=4
set expandtab

set ruler	    
set undolevels=1000
set backspace=indent,eol,start

" Lightline requirement
set laststatus=2

" VimWiki requirement
set nocompatible
filetype plugin on
syntax on

" Leader key = space
let mapleader = " "

" Specify a directory for plugins
" Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Vim-Plug
Plug 'mhinz/vim-startify'
Plug 'itchyny/lightline.vim'
Plug 'ap/vim-css-color'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'vimwiki/vimwiki'
Plug 'spf13/vim-autoclose'

" Initialize plugin system
call plug#end()

" Startify Custom header

let g:startify_custom_header = [     
	\ '                                       ',
	\ '    ░█▀▄░▄▀▀▄░▀█▀░█▀▀░░▀░░█░░█▀▀░█▀▀   ', 
	\ '    ░█░█░█░░█░░█░░█▀░░░█▀░█░░█▀▀░▀▀▄   ',
	\ '    ░▀▀░░░▀▀░░░▀░░▀░░░▀▀▀░▀▀░▀▀▀░▀▀▀   ',
	\ '     ---------- rphl 2020 ----------   ']
