#!/bin/bash

# FILES
read -p "Copy config files ? (y/n): " repcopy
if [ "$repcopy" = "y" ]
then
	cp ~/.conkyrc ~/Public/Fedoracula/home/.conkyrc
	cp ~/.zshrc ~/Public/Fedoracula/home/.zshrc
	cp ~/.config/neofetch/config.conf ~/Public/Fedoracula/.config/neofetch/config.conf
	cp ~/.config/rofi/config.rasi ~/Public/Fedoracula/.config/rofi/config.rasi
    cp -avr ~/.themes ~/Public/Fedoracula/.themes
    cp -avr ~/.local/share/xed/styles/ ~/Public/Fedoracula/.local/share/xed/styles/
	cp ~/bin/xfcepanel-cpu.sh ~/Public/Fedoracula/bin/xfcepanel-cpu.sh
	cp ~/bin/xfcepanel-mem.sh ~/Public/Fedoracula/bin/xfcepanel-mem.sh
	cp ~/bin/xfcepanel-hdd.sh ~/Public/Fedoracula/bin/xfcepanel-hdd.sh
	cp ~/bin/termcolors-light.sh ~/Public/Fedoracula/bin/termcolors-light.sh
	sleep 1s
	cd ~/Public/Fedoracula/ ; echo -e "List:" "\033[0;34m" `la` "\033[0m"
else
	exit
fi

# COMMIT / PUSH

read -p "Push to Git ? (y/n): " reppush
if [ "$reppush" = "y" ]
then
	read -r -p "Message:" gitdesc
	cd ~/Public/Fedoracula/ ; git add . ; git commit -m "$gitdesc" ; git push -u origin master
	sleep 1s
else
	sleep 1s
fi
