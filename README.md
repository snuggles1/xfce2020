# Xfce 2020
Fedora XFCE 2020 dotfiles

![Preview4](Preview4.png "Preview / Rphl")

## Softwares
__TYPE__  | __PROGRAM__ | __CONFIG__
----------|-------------|-----------
__Shell__|Zsh|[.zshrc](https://gitlab.com/rphl_m/xfce2020/tree/master/home/)
__Simple text Editor__|Xed|[Colorschemes XML](https://gitlab.com/rphl_m/xfce2020/tree/master/Configfiles/colorschemes-xml)
__Terminal text editor__|Vim + Plugins|[vimrc](https://gitlab.com/rphl_m/xfce2020/tree/master/home/.vimrc)
__Text Editor GUI__|Atom|*dnf*
__Terminal__|Xfce4-Terminal|[Colorschemes Theme](https://gitlab.com/rphl_m/xfce2020/tree/master/Configfiles/colorschemes-theme)
__Terminal Drop-down__|Guake|*dnf*
__Desktop System Info__|Conky|[.conkyrc](https://gitlab.com/rphl_m/xfce2020/tree/master/home/)
__System monitor__|Gtop|[Github](https://github.com/aksakalli/gtop)
__System Info light__|Ufetch|[Ufetch](https://gitlab.com/rphl_m/xfce2020/tree/master/home/bin/ufetch/)
__System Info__|Neofetch|[config.conf](https://gitlab.com/rphl_m/xfce2020/tree/master/home/.config/neofetch/)
__File Manager (term)__|Ranger|*dnf*
__File Manager__|Thunar|*dnf*
__Launcher__|Rofi|[config.rasi](https://gitlab.com/rphl_m/xfce2020/tree/master/home/.config/rofi/)
__Music player__|Cmus|*dnf* (requires RPM Fusion repo)
__Music Tag__|Exfalso|*dnf*
__Color picker__|Gpick|*dnf*
__Backup__|Grsync|*dnf*
__Numlock__|numlockx|*dnf*
__Screen color temperature__|redshift|*dnf*
__Search tool__|fd|*dnf*

## Repository
Enable repository RPM Fusion Free for additionnal packages:
```
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
```
Installation:
```
sudo dnf install atom bmon cmus conky emacs exfalso fd-find git googler gpick grsync guake neofetch numlockx ranger redshift ripgrep rofi xed zsh
```
Gtop requires NPM

## Appearance
__Type__  | __Program__ | __Config / Install__
----------|-------------|-----------
__Fonts Desktop__|Google-Roboto-Fonts|*dnf*
__Fonts Mono 1__|Iosevka|[Github](https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Iosevka)
__Fonts Mono 2__|JetBrainsMono|[JetBrains](https://www.jetbrains.com/lp/mono/)
__GTK Theme__|Adwaita-Dark|-
__GTK Theme alternative__|Juno|[Github](https://github.com/EliverLara/Juno)
__Xfwm 1__|Border-only|[Website](https://www.xfce-look.org/p/1016214)
__Xfwm 2__|Greybird-Accessibility-Dark|[Github](https://github.com/shimmerproject/Greybird/releases)
__Icons Theme__|Papirus-Icon-Theme|*dnf*
__Icons Theme colors__|Papirus-Fodlers|[Github](https://github.com/PapirusDevelopmentTeam/papirus-folders)
__Sounds__|System and events|Borealis*
__Wallpaper__|Gruvbox colors|[Wall](https://gitlab.com/rphl_m/xfce2020/tree/master/wallpaper-gruvbox.png)

```
sudo dnf install google-roboto-fonts materia-gtk-theme papirus-icon-theme
```

\* Extract `/Borealis` in `~l/.local/share/sounds/`, enable events & system sounds in `Appearance`, set "Borealis" as your theme in `Settings Editor > Xsettings > SoundThemeName`

## General Instructions
- __General instructions__: https://medium.com/tech-notes-and-geek-stuff/fedora-setup-routine-updated-eee084e856e
- __Zsh installation__: https://medium.com/tech-notes-and-geek-stuff/install-zsh-on-arch-linux-manjaro-and-make-it-your-default-shell-b0098b756a7a

## Additional tools

### Vivaldi browser
Aka the best browser in town:
```
$ sudo dnf config-manager --add-repo https://repo.vivaldi.com/archive/vivaldi-fedora.repo
$ sudo dnf install vivaldi-stable
```
If you get a duplicate repo configuration error, check if therepository vivaldi is listed more than once in the configuration folder:
```
$ grep vivaldi /etc/yum.repos.d/*
```
If two repos were configured you can delete the "Vivaldi-Fedora" one.

### OnlyOffice
Install from Flathub:
```
flatpak install flathub org.onlyoffice.desktopeditors
```
### Rofi Launcher

Explore the themes by running `rofi-theme-selector`, then hit Alt+a to apply.
To customize your launcher, copy the default config file and modify it:
```
$ rofi -dump-config > ~/.config/rofi/config.rasi
```

### ProtonVPN on Linux
Install and configure ProtonVPN:
```
$ sudo dnf install -y openvpn dialog python3-pip python3-setuptools
$ sudo pip3 install protonvpn-cli
```
You'll need your ProtonVPN credential to go further: type the following command, required information are located on your PM account: https://account.protonvpn.com/account
```
$ sudo protonvpn init
```
Once installed and logged, commands:
```
$ protonvpn c (connect)
$ protonvpn s (status)
$ protonvpn c -f (connect to fatest)
```
## XFCE4-panel
*    Workspaces
*    Button Windows
*    {separator}
*    Notification area
*    Audio
*    Battery
*    Notifications
*    launcher: xfce4-session-logout
*    Orage calendar: date
*    Orage calendar: hour
